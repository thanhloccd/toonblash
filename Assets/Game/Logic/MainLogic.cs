﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Core.DesignPattern;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class MainLogic : Singleton<MainLogic>
{
    #region ConstVariable

    private const int Width = 9; //total number of bricks column-wise
    private const int Height = 9; //total number of bricks row-wise
    private const float BrickHeight = 0.5f;
    private const float BrickWidth = 0.5f;
    private const int Undefined = -1;
    private const int CreateBomb = 6;
    private const int CreateMissile = 4;
    private const int CorruptBrickCreate = 0;
    private const int ScoreEachBlock = 50;
    private const int ScoreBonus = 200;
    private const int Time = 120;
    private const int ScoreWin = 3000;
    #endregion

    #region PrivateVariable

    private int _corruptBrickNum = 6;
    private int _move;
    private Transform[][] _grid = new Transform[Width][];
    private IEnumerator _fallElementsDownCoroutine;
    private int _score;
    private int _blockCounter;
    private List<string> _colorNames;
    private bool _crRunning;
    private bool _elementsAreCreated;
    private bool _changeHappen;
    private List<Point> _visitedBomb = new List<Point>();
    private float _timeLeft;
    private bool _isGameOver;
    #endregion

    #region SerializedVariable

    [FormerlySerializedAs("blocks")] [SerializeField] private GameObject[] _blocks;
    [FormerlySerializedAs("missiles")] [SerializeField] private GameObject[] _missiles;
    [FormerlySerializedAs("bomb")] [SerializeField] private GameObject _bomb;
    [FormerlySerializedAs("corruptedBrick")] [SerializeField] private GameObject _corruptedBrick;
    [SerializeField] private AudioSource _audioSource;
    [SerializeField] private AudioClip _clipExplosionBoom;
    [SerializeField] private AudioClip _clipExplosion;
    [SerializeField] private AudioClip _clipPing;
    #endregion

    #region Mono

    private void Start()
    {
        Init();
    }

    private void Update()
    {
        var bricksToBeAdded = new Dictionary<int, int>();
        var gridFull = true;
        if (!_crRunning)
        {
            gridFull = CheckGridNotNeedNewBrick(bricksToBeAdded);
        }

        if (!gridFull && !_crRunning)
        {
            _elementsAreCreated = false;
            _visitedBomb = new List<Point>();
            _crRunning = true;
            _fallElementsDownCoroutine = FallElementsDown(bricksToBeAdded);
            StartCoroutine(_fallElementsDownCoroutine);
            _move++;
            _changeHappen = true;
        }

        if (CorruptBrickCreate != 0 && _move % CorruptBrickCreate == 0 && _elementsAreCreated)
        {
            _elementsAreCreated = false;
            MakeRandomBricksCorrupted();
        }
        
        if (!_crRunning && _changeHappen)
        {
            // isGameOver = !CheckMoveAvailable();
            _changeHappen = false;
        }
        
    }

    #endregion

    #region Init

    private void Init()
    {
        for (int i = 0; i < _grid.Length; i++)
        {
            _grid[i] = new Transform[Height];
        }

        _colorNames = new List<string>
        {
            "blue",
            "red",
            "green",
            "yellow",
            "orange"
        };
        _score = 0;
        _timeLeft = Time;
        FillContainer();
        InvokeRepeating(nameof(CountTime),1,1);
    }

    /// <summary>
    /// Fills container with random generated bricks.
    /// </summary>
    private void FillContainer()
    {
        for (var i = 0; i < Width; i++)
        {
            for (var j = 0; j < Height; j++)
            {
                var position = transform.position + new Vector3(i * BrickWidth, j * BrickHeight, 0);
                var newBlock = Instantiate(_blocks[Random.Range(0, _blocks.Length)], position,
                    Quaternion.identity);
                newBlock.transform.SetParent(gameObject.transform);
                newBlock.name = _blockCounter++.ToString();
                _grid[i][j] = newBlock.transform;
            }
        }
    }

    #endregion

    #region Grid

    /// <summary>
    /// Checks grids are do not need new bricks
    /// Adds the missing bricks column information
    /// </summary>
    private bool CheckGridNotNeedNewBrick(Dictionary<int, int> bricksToBeAdded)
    {
        int counter = 0;
        for (int i = 0; i < Width; i++)
        {
            for (int j = 0; j < Height; j++)
            {
                if (ReferenceEquals(_grid[i][j], null))
                {
                    counter++;
                    if (bricksToBeAdded.ContainsKey(i))
                    {
                        bricksToBeAdded[i] += 1;
                    }
                    else
                    {
                        bricksToBeAdded.Add(i, 1);
                    }
                }
            }
        }

        //Bomb creating also triggers so...
        return counter < 2;
    }

    /// <summary>
    /// Check colorNames have available value
    /// </summary>
    /// <returns></returns>
    private bool NoColorFulLeft()
    {
        for (var i = 0; i < Width; i++)
        for (var j = 0; j < Height; j++)
        {
            if (_colorNames.Contains(_grid[i][j].transform.gameObject.tag))
                return false;
        }

        return true;
    }

    private void FindAndDeleteElements(Transform clickedObject)
    {
        int clickedBlockX = Undefined, clickedBlockY = Undefined;

        GetClickedGrid(ref clickedBlockX, ref clickedBlockY, clickedObject);

        if (clickedBlockX != Undefined)
        {
            string clickedColor = _grid[clickedBlockX][clickedBlockY].tag;
            List<Point> elementsToBeTraversed = new List<Point>();
            List<Point> elementsToBeDeleted = new List<Point>();
            elementsToBeTraversed.Add(new Point(clickedBlockX, clickedBlockY));
            elementsToBeDeleted.Add(new Point(clickedBlockX, clickedBlockY));

            Dictionary<int, int> missingBricksAtColumns = new Dictionary<int, int>();
            missingBricksAtColumns.Add(clickedBlockX, 1);

            TraverseNew(elementsToBeTraversed, clickedColor, elementsToBeDeleted, missingBricksAtColumns);

            if (elementsToBeDeleted.Count < 2)
            {
                _audioSource.PlayOneShot(_clipPing);
                clickedObject.DOShakePosition(0.5f,0.1f,20);
                return;
            }
            
            AddScore(elementsToBeDeleted.Count);

            DeleteElements(elementsToBeDeleted, ShouldMissileBeCreated(elementsToBeDeleted), missingBricksAtColumns);
        }
    }


    private void AddElement(int x, int y, List<Point> deleteList, Dictionary<int, int> dictionary)
    {
        if (x > -1 && x < Width && y > -1 && y < Height)
        {
            Point toAdd = new Point(x, y);
            if (!ReferenceEquals(_grid[x][y], null) && !deleteList.Contains(toAdd))
            {
                deleteList.Add(toAdd);
                if (dictionary.ContainsKey(x))
                {
                    dictionary[x] += 1;
                }
                else
                {
                    dictionary.Add(x, 1);
                }
            }
        }
    }

    private void DeleteElements(List<Point> elementsToBeDeleted, bool createBomb, Dictionary<int, int> dictionary)
    {
        foreach (Point point in elementsToBeDeleted)
        {
            Vector3 pos = new Vector3(0, 0, 0);
            if (createBomb)
            {
                createBomb = false;
                pos = _grid[point.GetX()][point.GetY()].gameObject.transform.position;
                Destroy(_grid[point.GetX()][point.GetY()].gameObject);
                var bombObject = elementsToBeDeleted.Count > CreateBomb
                    ? Instantiate(_bomb, pos, Quaternion.identity)
                    : Instantiate(_missiles[Random.Range(0, _missiles.Length)], pos, Quaternion.identity);
                bombObject.name = _blockCounter++.ToString();
                _grid[point.GetX()][point.GetY()] = bombObject.transform;
                dictionary[point.GetX()] -= 1;
            }
            else
            {
                string brickId = _grid[point.GetX()][point.GetY()].gameObject.name;
                var bombOrBrick = GameObject.Find(brickId).GetComponent<Collectable>();
                bombOrBrick.Trigger(point.GetX(), point.GetY());
                _audioSource.PlayOneShot(_clipExplosion);
            }
        }
    }

    private void CreateColumns(List<int> mc)
    {
        foreach (var t in mc)
        {
            UnityEngine.Object newBlock = Instantiate(_blocks[Random.Range(0, _blocks.Length)],
                new Vector3(transform.position.x + t * BrickWidth, transform.position.y + (Height - 1) * BrickHeight,
                    0), Quaternion.identity);
            GameObject gameObjectBlock = (GameObject)newBlock;
            newBlock.name = "" + _blockCounter++;
            gameObjectBlock.transform.SetParent(gameObject.transform);
            _grid[t][Height - 1] = gameObjectBlock.transform;
        }
    }

    public void DeleteFromGrid(int x, int y)
    {
        if (x != -1)
            _grid[x][y] = null;
    }

    private void GetClickedGrid(ref int x, ref int y, Transform clickedObject)
    {
        for (int i = 0; i < Width; i++)
        {
            for (int j = 0; j < Height; j++)
            {
                if (ReferenceEquals(_grid[i][j], null))
                {
                    continue;
                }

                if (_grid[i][j].Equals(clickedObject))
                {
                    x = i;
                    y = j;
                    break;
                }
            }
        }
    }

    #endregion

    #region Effect

    private IEnumerator FallElementsDown(Dictionary<int, int> dictionary)
    {
        while (true)
        {
            bool stillFalling = true;
            bool allFilled = true;
            for (int i = 0; i < dictionary.Count; i++)
            {
                if (dictionary.ElementAt(i).Value != 0)
                {
                    allFilled = false;
                    break;
                }
            }

            if (allFilled)
            {
                _elementsAreCreated = true;
                break;
            }

            while (stillFalling)
            {
                yield return new WaitForSeconds(0.01f);
                stillFalling = false;
                for (int x = 0; x < Width; x++)
                {
                    for (int y = 0; y < Height; y++)
                    {
                        if (!ReferenceEquals(_grid[x][y], null)) continue;
                        for (int indexY = y + 1; indexY < Height; indexY++)
                        {
                            if (!ReferenceEquals(_grid[x][indexY], null))
                            {
                                stillFalling = true;
                                _grid[x][indexY - 1] = _grid[x][indexY];
                                Vector2 vector = _grid[x][indexY - 1].transform.position;
                                vector.y -= BrickHeight;
                                _grid[x][indexY - 1].transform.position = vector;
                                _grid[x][indexY] = null;
                            }
                        }
                    }
                }
            }

            BringNewBricks(dictionary);
        }


        _crRunning = false;
    }

    #endregion

    #region Brick

    public void GetClickedBrick(Transform clickedTransform)
    {
        if (!_crRunning && !_isGameOver)
        {
            FindAndDeleteElements(clickedTransform);
        }
    }

    private void MakeRandomBricksCorrupted()
    {
        int corruptedSelected = 0;
        while (corruptedSelected < _corruptBrickNum)
        {
            if (NoColorFulLeft())
            {
                _isGameOver = true;
                break;
            }

            int randomHeight = Random.Range(0, Height);
            int randomWidth = Random.Range(0, Width);


            if (_colorNames.Contains(_grid[randomWidth][randomHeight].gameObject.tag))
            {
                corruptedSelected++;
                var vector = _grid[randomWidth][randomHeight].position;
                Destroy(_grid[randomWidth][randomHeight].gameObject);
                var corruptedObject = Instantiate(_corruptedBrick, vector, Quaternion.identity);
                corruptedObject.name = "corrupted" + _blockCounter;
                _blockCounter++;
                _grid[randomWidth][randomHeight] = corruptedObject.transform;
            }
        }

        if (_corruptBrickNum < 20)
            _corruptBrickNum++;
    }

    private void BringNewBricks(Dictionary<int, int> dictionary)
    {
        var list = new List<int>();
        for (var i = 0; i < dictionary.Count; i++)
        {
            var item = dictionary.ElementAt(i);
            if (item.Value == 0) continue;
            list.Add(item.Key);
            dictionary[item.Key] -= 1;
        }

        CreateColumns(list);
    }

    #endregion

    #region Logic

    /// <summary>
    /// Check User can move
    /// </summary>
    /// <returns></returns>
    private bool CheckMoveAvailable()
    {
        //Move
        for (int i = 0; i < Width; i++)
        for (int j = 0; j < Height; j++)
        {
            string clickedColor = _grid[i][j].tag;
            if (clickedColor.Equals("bomb") || clickedColor.Equals("missile") || clickedColor.Equals("upmissile"))
            {
                return true;
            }

            if (clickedColor.Equals("corrupted"))
                continue;
            List<Point> elementsToBeTraversed = new List<Point>();
            List<Point> elementsToBeDeleted = new List<Point>();
            elementsToBeTraversed.Add(new Point(i, j));
            elementsToBeDeleted.Add(new Point(i, j));

            Dictionary<int, int> missingBricksAtColumns = new Dictionary<int, int>();
            missingBricksAtColumns.Add(i, 1);

            TraverseNew(elementsToBeTraversed, clickedColor, elementsToBeDeleted, missingBricksAtColumns);

            if (elementsToBeDeleted.Count > 1) return true;
        }
        EndGame(false);
        return false;
    }

    private void TraverseNew(List<Point> elementsToBeTraversed, string color, List<Point> elementsToBeDeleted,
        Dictionary<int, int> dictionary)
    {
        while (elementsToBeTraversed.Count > 0)
        {
            int curX = elementsToBeTraversed[0].GetX();
            int curY = elementsToBeTraversed[0].GetY();
            CheckElement(curX - 1, curY);
            CheckElement(curX + 1, curY);
            CheckElement(curX, curY + 1);
            CheckElement(curX, curY - 1);

            elementsToBeTraversed.Remove(elementsToBeTraversed[0]);
        }

        void CheckElement(int x, int y)
        {
            if (x > -1 && x < Width && y > -1 && y < Height)
            {
                if (!ReferenceEquals(_grid[x][y], null) && _grid[x][y].tag.Equals(color))
                {
                    Point newCur = new Point(x, y);
                    if (!elementsToBeDeleted.Contains(newCur) && !elementsToBeTraversed.Contains(newCur))
                    {
                        if (dictionary.ContainsKey(newCur.GetX()))
                        {
                            dictionary[newCur.GetX()] += 1;
                        }
                        else
                        {
                            dictionary.Add(newCur.GetX(), 1);
                        }

                        elementsToBeDeleted.Add(newCur);
                        elementsToBeTraversed.Add(newCur);
                    }
                }
            }
        }
    }

    private void EndGame(bool isWin) => Instance.StartCoroutine(EndGameCoroutine(isWin));
    private IEnumerator EndGameCoroutine(bool isWin)
    {
        print("End Game");
        yield return new WaitUntil(() => !_crRunning);
        print("Win:"+isWin);
        if (isWin)
        {
            GamePlayUIController.Instance.OpenPanelWin();
            if(_score>PlayerPrefs.GetInt("BestScore",0))
                PlayerPrefs.SetInt("BestScore",_score);
        }
        else
        {
            GamePlayUIController.Instance.OpenPanelLose();
        }
    }
    #endregion

    #region Missile

    private bool ShouldMissileBeCreated(List<Point> elementsToBeDeleted)
    {
        return elementsToBeDeleted.Count > CreateMissile;
    }

    public void GetMissiledBrick(Transform gameObjectTransform)
    {
        if (!_crRunning && !_isGameOver)
        {
            MissileIt(gameObjectTransform);
        }
    }

    public void GetMissiledBrickUpside(Transform gameObjectTransform)
    {
        if (!_crRunning && !_isGameOver)
        {
            MissileItUpside(gameObjectTransform);
        }
    }

    public void MissileIt(Transform gameObjectTransform)
    {
        int x = Undefined, y = Undefined;
        GetClickedGrid(ref x, ref y, gameObjectTransform);
        List<Point> elementsToDelete = new List<Point>();
        var dictionary = new Dictionary<int, int>();
        var listBomb = new List<Point>();
        listBomb.Add(new Point(x, y));

        FindMissiledElements(listBomb, dictionary, elementsToDelete);

        DeleteElements(elementsToDelete, false, dictionary);
        AddScore(elementsToDelete.Count);
    }

    public void MissileItUpside(Transform gameObjectTransform)
    {
        int x = Undefined, y = Undefined;
        GetClickedGrid(ref x, ref y, gameObjectTransform);
        List<Point> elementsToDelete = new List<Point>();
        var dictionary = new Dictionary<int, int>();
        var listBomb = new List<Point>();
        listBomb.Add(new Point(x, y));

        FindMissiledElementsUpside(listBomb, dictionary, elementsToDelete);

        DeleteElements(elementsToDelete, false, dictionary);
        AddScore(elementsToDelete.Count);
    }

    private void FindMissiledElementsUpside(List<Point> listBomb, Dictionary<int, int> dictionary,
        List<Point> elementsToDelete)
    {
        _visitedBomb.Add(new Point(listBomb[0].GetX(), listBomb[0].GetY()));
        var x = listBomb[0].GetX();
        for (int i = 0; i < Height; i++)
        {
            if (!_visitedBomb.Contains(new Point(x, i)) &&
                _grid[x][i].gameObject.GetComponent(typeof(GameElement)) != null)
            {
                String gameObjectId = _grid[x][i].transform.gameObject.name;
                GameElement gameElement = GameObject.Find(gameObjectId).GetComponent<GameElement>();
                gameElement.OnMouseDown();
            }

            AddElement(x, i, elementsToDelete, dictionary);
        }
    }

    private void FindMissiledElements(List<Point> listBomb, Dictionary<int, int> dictionary,
        List<Point> elementsToDelete)
    {
        var y = listBomb[0].GetY();
        _visitedBomb.Add(new Point(listBomb[0].GetX(), listBomb[0].GetY()));
        for (int i = 0; i < Width; i++)
        {
            if (!_visitedBomb.Contains(new Point(i, y)) &&
                _grid[i][y].gameObject.GetComponent(typeof(GameElement)) != null)
            {
                String gameObjectId = _grid[i][y].transform.gameObject.name;
                GameElement gameElement = GameObject.Find(gameObjectId).GetComponent<GameElement>();
                gameElement.OnMouseDown();
            }

            AddElement(i, y, elementsToDelete, dictionary);
        }
    }

    #endregion
    
    #region Boom

    public void GetBombedBrick(Transform gameObjectTransform)
    {
        if (!_crRunning && !_isGameOver)
        {
            BombIt(gameObjectTransform);
            _audioSource.PlayOneShot(_clipExplosionBoom);
        }
    }

    public void BombIt(Transform gameObjectTransform)
    {
        int x = Undefined, y = Undefined;
        GetClickedGrid(ref x, ref y, gameObjectTransform);
        List<Point> elementsToDelete = new List<Point>();
        var dictionary = new Dictionary<int, int>();
        var listBomb = new List<Point>();
        listBomb.Add(new Point(x, y));

        FindBombedElements(listBomb, dictionary, elementsToDelete);

        DeleteElements(elementsToDelete, false, dictionary);
        AddScore(elementsToDelete.Count);
    }

    private void FindBombedElements(List<Point> listBomb, Dictionary<int, int> dictionary, List<Point> elementsToDelete)
    {
        _visitedBomb.Add(listBomb[0]);
        while (listBomb.Count > 0)
        {
            for (int i = listBomb[0].GetX() - 1; i <= listBomb[0].GetX() + 1; i++)
            {
                for (int j = listBomb[0].GetY() - 1; j <= listBomb[0].GetY() + 1; j++)
                {
                    if (i < 0 || i >= Width || j < 0 || j >= Height)
                        continue;
                    AddElement(i, j, elementsToDelete, dictionary);
                    Point point = new Point(i, j);
                    if (!_visitedBomb.Contains(point) &&
                        _grid[i][j].gameObject.GetComponent(typeof(GameElement)) != null)
                    {
                        String gameObjectId = _grid[i][j].transform.gameObject.name;
                        GameElement gameElement = GameObject.Find(gameObjectId).GetComponent<GameElement>();
                        gameElement.OnMouseDown();
                    }
                }
            }

            if (listBomb.Count > 0)
            {
                listBomb.RemoveAt(0);
            }
        }
    }

    #endregion
    
    #region UI

    private void AddScore(int count)
    {
        var scoreToAdd = ScoreEachBlock * count;
        if (count > 5)
            scoreToAdd += ScoreBonus;
        _score += scoreToAdd;
        GamePlayUIController.Instance.SetScore(_score);
        if (_score > ScoreWin)
        {
            _isGameOver = true;
            EndGame(true);
        }
    }

    private void CountTime()
    {
        switch (_timeLeft)
        {
            case > 0 when !_isGameOver:
                _timeLeft -= 1;
                break;
            case <= 0:
                _isGameOver = true;
                EndGame(false);
                break;
        }

        GamePlayUIController.Instance.SetTime(_timeLeft);
    }
    
    #endregion
}

struct Point
{
    int x;
    int y;

    public Point(int x, int y)
    {
        this.x = x;
        this.y = y;
    }

    public int GetX()
    {
        return x;
    }

    public int GetY()
    {
        return y;
    }
}