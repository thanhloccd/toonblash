using System.Collections;
using System.Collections.Generic;
using Core.DesignPattern;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;
using UnityEngine.SocialPlatforms.Impl;
using UnityEngine.UI;

public class GamePlayUIController : Singleton<GamePlayUIController>
{
    #region SerializedVariable
    
    [Space,Header("UI")]
    [SerializeField] private TextMeshProUGUI _txtScore;
    [SerializeField] private TextMeshProUGUI _txtTime;
    [SerializeField] private List<Button> _btnsRestart;
    [SerializeField] private List<Button> _btnsMainMenu;

    [Space, Header("PanelConfirmBack")] 
    [SerializeField] private Button _btnBack;
    [SerializeField] private Button _btnCancel;
    [SerializeField] private GameObject _panelConfirmBack;
    
    [Space, Header("PanelWin")] 
    [SerializeField] private TextMeshProUGUI _txtWinScore;
    [SerializeField] private TextMeshProUGUI _txtWinBestScore;
    [SerializeField] private TextMeshProUGUI _txtWinTime;
    [SerializeField] private GameObject _panelWin;
    
    [Space, Header("PanelLose")] 
    [SerializeField] private TextMeshProUGUI _txtLoseScore;
    [SerializeField] private GameObject _panelLose;
    #endregion

    #region Mono

    protected override void Awake()
    {
        base.Awake();
        foreach (var btn in _btnsRestart)      
        {
            btn.onClick.AddListener(Restart);
        }

        foreach (var btn in _btnsMainMenu)
        {
            btn.onClick.AddListener(MainMenu);
        }
        
        _btnBack.onClick.AddListener(()=>OpenPanel(_panelConfirmBack));
        _btnCancel.onClick.AddListener(()=>_panelConfirmBack.SetActive(false));
    }

    #endregion

    #region Control

    private void Restart()
    {
        SceneManager.LoadScene("Game");
    }

    private void MainMenu()
    {
        SceneManager.LoadScene("Menu");
    }

    #endregion

    #region UI

    private void OpenPanel(GameObject panel)
    {
        _panelLose.SetActive(false);
        _panelConfirmBack.SetActive(false);
        _panelWin.SetActive(false);
        
        panel.SetActive(true);
    }
    
    public void SetScore(int score)
    {
        _txtScore.text = string.Format("Score: {0}", score);
    }

    public void SetTime(float time)
    {
        _txtTime.text = string.Format("Time: {0:00}s", time);
    }

    public void OpenPanelWin()
    {
        OpenPanel(_panelWin);
        _txtWinScore.text = _txtScore.text;
        _txtWinBestScore.text=string.Format("Best Score: {0}", PlayerPrefs.GetInt("BestScore",0));
        _txtWinTime.text = _txtTime.text;
    }

    public void OpenPanelLose()
    {
        OpenPanel(_panelLose);
        _txtLoseScore.text = _txtScore.text;
    }
    #endregion
}
