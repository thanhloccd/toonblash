using UnityEngine;

public abstract class GameElement : MonoBehaviour 
{
    #region Mono

    public abstract void OnMouseDown();

    #endregion
    
}
