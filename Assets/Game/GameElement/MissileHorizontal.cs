﻿using UnityEngine;

public class MissileHorizontal : GameElement
{
    #region Mono

    public override void OnMouseDown()
    { 
        MainLogic.Instance.GetMissiledBrick(gameObject.transform);
    }

    #endregion
    
}
