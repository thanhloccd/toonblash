﻿using System.Collections.Generic;
using UnityEngine;

public class Collectable : MonoBehaviour
{
    #region PrivateVariable

    private int _x = -1;
    private int _y = -1;
    private readonly int AniDestroy1 = Animator.StringToHash("destroy");

    #endregion

    #region Mono

    private void OnDestroy()
    {
        MainLogic.Instance.DeleteFromGrid(_x,_y);
    }

    #endregion

    #region Control

    public void Trigger(int x, int y)
    {
        var anim = GetComponent<Animator>();
        anim.SetTrigger(AniDestroy1);
        _x = x;
        _y = y;
    }

    #endregion
    
    
}
