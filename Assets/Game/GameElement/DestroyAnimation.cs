﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyAnimation : StateMachineBehaviour
{
    #region Mono

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        Destroy(animator.gameObject, stateInfo.length);
    }

    #endregion
    
}
