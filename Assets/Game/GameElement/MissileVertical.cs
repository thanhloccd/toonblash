﻿
using UnityEngine;

public class MissileVertical : GameElement
{
    #region Mono

    public override void OnMouseDown()
    {
        MainLogic.Instance.GetMissiledBrickUpside(gameObject.transform);
    }

    #endregion
    
}