using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainUIController : MonoBehaviour
{
    #region SerializedVariable

    [SerializeField] private Button _btnPlay;

    #endregion

    #region Mono

    private void Awake()
    {
        _btnPlay.onClick.AddListener(Play);
    }

    #endregion

    #region Control

    private void Play()
    {
        SceneManager.LoadScene("Game");
    }
    
    #endregion
}
